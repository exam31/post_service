package postgres

import (
	pb "exam/post_service/genproto/post"
	"fmt"

	"github.com/jmoiron/sqlx"
)

type postRepo struct {
	db *sqlx.DB
}

func NewPostRepo(db *sqlx.DB) *postRepo {
	return &postRepo{db: db}
}

// Create(*pb.PostReq) (*pb.PostResp, error)
func (r *postRepo) Create(post *pb.PostReq) (*pb.PostResp, error) {
	tr, _ := r.db.Begin()
	defer tr.Rollback()
	responsePost := pb.PostResp{}
	err := tr.QueryRow(`insert into posts(
		owner_id,
		name,
		description) 
		values($1, $2, $3) 
		returning id, 
		owner_id, 
		name,
		description,
		created_at`,
		post.OwnerId,
		post.Name,
		post.Description).Scan(
		&responsePost.Id,
		&responsePost.OwnerId,
		&responsePost.Name,
		&responsePost.Description, &responsePost.CreatedAt)
	if err != nil {
		tr.Rollback()
		fmt.Println("error while inserting posts")
		return &pb.PostResp{}, err
	}
	var medias []*pb.Media
	for _, media := range post.Medias {
		mediaResp := pb.Media{}
		media.PostId = responsePost.Id
		err = tr.QueryRow(`insert into medias(post_id, name, link, type)
			values($1,$2,$3,$4) returning id, post_id, name, link, type`,
			media.PostId, media.Name, media.Link, media.Type).Scan(
			&mediaResp.Id,
			&mediaResp.PostId,
			&mediaResp.Name,
			&mediaResp.Link,
			&mediaResp.Type,
		)
		if err != nil {
			tr.Rollback()
			fmt.Println("error while inserting media")
			return &pb.PostResp{}, err
		}
		medias = append(medias, &mediaResp)
	}
	responsePost.Medias = medias

	if err = tr.Commit(); err != nil {
		fmt.Println("error tr.Commit()", err)
	}
	return &responsePost, nil
}

// GetPostReview(*pb.ID) (*pb.PostInfo,error)
func (r *postRepo) GetPostReview(post *pb.ID) (*pb.PostInfo, error) {
	response := pb.PostInfo{}
	err := r.db.QueryRow(`SELECT  
		id, 
		owner_id, name, 
		description,created_at, 
		updated_at FROM  posts WHERE 
		id=$1 AND deleted_at IS NULL`, post.PostID).Scan(
		&response.Id, &response.OwnerId, &response.Name,
		&response.Description, &response.CreatedAt, &response.UpdatedAt,
	)
	if err != nil {
		fmt.Println("error while getting post with review")
		return &pb.PostInfo{}, err
	}
	rows, err := r.db.Query(`select id, post_id, name, link, type from medias where post_id=$1`, post.PostID)
	if err != nil {
		fmt.Println("error while getting media")
		return &pb.PostInfo{}, err
	}
	defer rows.Close()
	for rows.Next() {
		media := pb.Media{}
		err = rows.Scan(&media.Id, &media.PostId, &media.Name, &media.Link, &media.Type)
		if err != nil {
			fmt.Println("error while scanning media")
			return &pb.PostInfo{}, err
		}
		response.Medias = append(response.Medias, &media)
	}
	return &response, nil
}

// GetPostCustomer(*pb.GetCustomerPostRequest) (*pb.Posts,error)
func (r *postRepo) GetPostCustomer(OwnerId string) (*pb.Posts, error) {
	posts := pb.Posts{}
	rowsPost, err := r.db.Query(`select 
	id, owner_id, 
	name, description, 
	created_at, updated_at 
	from posts where owner_id=$1 
	and deleted_at is null`, OwnerId)
	if err != nil {
		fmt.Println("error while getting post with customer")
		return &pb.Posts{}, err
	}
	for rowsPost.Next() {
		postRes := pb.PostResp{}
		err = rowsPost.Scan(
			&postRes.Id,
			&postRes.OwnerId, &postRes.Name,
			&postRes.Description, &postRes.CreatedAt, &postRes.UpdatedAt,
		)
		if err != nil {
			fmt.Println("error while scanning postinfo to customer")
		}
		rows, err := r.db.Query(`select id, post_id, name, link, type from medias where post_id=$1`, postRes.Id)

		if err != nil {
			fmt.Println("error while getting post medias to customer")
			return &pb.Posts{}, err
		}
		for rows.Next() {
			mediaRes := pb.Media{}
			err = rows.Scan(
				&mediaRes.Id, &mediaRes.PostId, &mediaRes.Name, &mediaRes.Link, &mediaRes.Type,
			)
			if err != nil {
				fmt.Println("error while scanning post medias to customer")
			}
			postRes.Medias = append(postRes.Medias, &mediaRes)
		}
		posts.Posts = append(posts.Posts, &postRes)
	}
	return &posts, nil
}

// UpdatePost(*pb.PostResp) (*pb.PostResp, error)
func (r *postRepo) UpdatePost(post *pb.PostUp) (*pb.PostResp, error) {
	response := pb.PostResp{}
	_, err := r.db.Exec(`UPDATE posts SET 
		owner_id=$1,
		name=$2,
		description=$3,
		updated_at=NOW()
		WHERE id=$4 and deleted_at IS NULL`, post.OwnerId, post.Name, post.Description, post.Id)
	if err != nil {
		fmt.Println("error while update post")
		return &pb.PostResp{}, err
	}
	for _, media := range post.Medias {
		_, err = r.db.Exec(`update medias set name=$1, link=$2, type=$3 where id=$4`, media.Name, media.Link, media.Type, media.Id)
		if err != nil {
			fmt.Println("error while update media")
			return &pb.PostResp{}, err
		}
	}
	err = r.db.QueryRow(`SELECT  
	id, 
	owner_id, name, 
	description, updated_at FROM  
	posts WHERE id=$1 AND deleted_at IS NULL`, post.Id).Scan(
		&response.Id, &response.OwnerId, &response.Name, &response.Description, &response.UpdatedAt)
	if err != nil {
		fmt.Println("error while getting posts in update")
	}
	rows, err := r.db.Query(`select id, post_id, name, link, type from medias where post_id=$1`, post.Id)
	if err != nil {
		fmt.Println("error while getting medias in update")
		return &pb.PostResp{}, err
	}
	defer rows.Close()
	for rows.Next() {
		media := pb.Media{}
		err = rows.Scan(&media.Id, &media.PostId, &media.Name, &media.Link, &media.Type)
		if err != nil {
			fmt.Println("error while scanning medias in update")
			return &pb.PostResp{}, err
		}
		response.Medias = append(response.Medias, &media)
	}
	return &response, nil
}

// DeletePost(*pb.ID) (*pb.Empty, error)
func (r *postRepo) DeletePost(post *pb.ID) (*pb.Empty, error) {
	response := pb.Empty{}
	err := r.db.QueryRow(`update posts set deleted_at=NOW() where id=$1 and deleted_at is null`, post.PostID).Err()
	if err != nil {
		fmt.Println("error delete posts")
		return &pb.Empty{}, err
	}
	return &response, nil
}

// DeleteCustomerPost(*pb.CustomerId) (*pb.Empty,error)
func (r *postRepo) DeleteCustomerPost(post *pb.CustomerId) (*pb.Empty, error) {
	response := pb.Empty{}
	err := r.db.QueryRow(`update posts set deleted_at=NOW() where owner_id=$1 and deleted_at is null`, post.OwnerId).Err()
	if err != nil {
		fmt.Println("error delete posts customers")
		return &pb.Empty{}, err
	}
	return &response, nil
}

func (r *postRepo) ListPost(value string, limit, page int64) (*pb.ListPostResponse, error) {
	posts := pb.ListPostResponse{}
	query := fmt.Sprintf("SELECT name, description FROM posts WHERE name LIKE '%s%%' OR description LIKE '%s%%' ORDER BY name  LIMIT %d OFFSET %d", value, value, limit, (page-1)*limit)
	rowsPost, err := r.db.Query(query)
	for rowsPost.Next() {
		post := pb.PostSearchResp{}
		err = rowsPost.Scan(
			&post.Name,
			&post.Description,
		)

		if err != nil {
			fmt.Println("error while scan post list")
			return &pb.ListPostResponse{}, err
		}
		posts.Posts = append(posts.Posts, &post)
	}
	return &posts, err
}
