CREATE TABLE IF NOT EXISTS posts(
    id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    owner_id UUID NOT NULL,
    name TEXT, 
    description TEXT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP
);