CREATE TABLE IF NOT EXISTS medias(
    id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    post_id UUID NOT NULL REFERENCES posts(id),
    name TEXT,
    link TEXT,
    type TEXT
);