package service

import (
	"context"
	pb "exam/post_service/genproto/post"
	rs "exam/post_service/genproto/review"
	l "exam/post_service/pkg/logger"
	grpcclient "exam/post_service/service/grpc_client"
	"exam/post_service/storage"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type PostService struct {
	storage storage.IStorage
	logger  l.Logger
	client  grpcclient.GrpcClientI
}

func NewPostService(db *sqlx.DB, log l.Logger, client grpcclient.GrpcClientI) *PostService {
	return &PostService{
		storage: storage.NewStoragePg(db),
		logger:  log,
		client:  client,
	}
}

// Create(*pb.PostReq) (*pb.PostResp, error)
func (s *PostService) Create(ctx context.Context, req *pb.PostReq) (*pb.PostResp, error) {
	post, err := s.storage.Post().Create(req)
	if err != nil {
		s.logger.Error("error insert post", l.Any("Error insert post", err))
		return &pb.PostResp{}, status.Error(codes.Internal, "something went wrong, please check post info")
	}
	return post, nil
}

// UpdatePost(*pb.PostResp) (*pb.PostResp, error)
func (s *PostService) UpdatePost(ctx context.Context, req *pb.PostUp) (*pb.PostResp, error) {
	post, err := s.storage.Post().UpdatePost(req)
	if err != nil {
		s.logger.Error("error update post", l.Any("error updating post", err))
		return &pb.PostResp{}, status.Error(codes.Internal, "sometging went wrong, please check post info")
	}
	return post, nil
}

// 	GetPostReview(*pb.ID) (*pb.PostInfo,error)
func (s *PostService) GetPostReview(ctx context.Context, req *pb.ID) (*pb.PostInfo, error) {
	post, err := s.storage.Post().GetPostReview(req)
	if err != nil {
		s.logger.Error("error select", l.Any("Error select post review", err))
		return &pb.PostInfo{}, status.Error(codes.Internal, "something went wrong, please check post info")
	}
	review, err := s.client.Review().GetReviewPost(ctx, &rs.GetReviewPostRequest{
		PostId: req.PostID,
	})
	if err != nil {
		s.logger.Error("error select", l.Any("Error select post review", err))
		return &pb.PostInfo{}, status.Error(codes.Internal, "something went wrong, please check post info")
	}
	for _, r := range review.Reviews {
		post.Reviews = append(post.Reviews, &pb.Review{
			Id:          r.Id,
			PostId:      r.PostId,
			OwnerId:     r.OwnerId,
			Name:        r.Name,
			Rating:      r.Rating,
			Description: r.Description,
			CreatedAt:   r.CreatedAt,
			UdpatedAt:   r.UdpatedAt,
		})
	}
	return post, nil
}

// 	GetPostCustomer(*pb.GetCustomerPostRequest) (*pb.Posts,error)
func (s *PostService) GetPostCustomer(ctx context.Context, req *pb.GetCustomerPostRequest) (*pb.Posts, error) {
	post, err := s.storage.Post().GetPostCustomer(req.OwnerId)
	if err != nil {
		s.logger.Error("error select post customer", l.Any("error select post customer", err))
		return &pb.Posts{}, status.Error(codes.Internal, "something went wrong, please check post info")
	}
	return post, nil
}

// DeletePost(*pb.ID) (*pb.Empty, error)
func (s *PostService) DeletePost(ctx context.Context, req *pb.ID) (*pb.Empty, error) {
	post, err := s.storage.Post().DeletePost(req)
	if err != nil {
		s.logger.Error("error update post", l.Any("error updating post", err))
		return &pb.Empty{}, status.Error(codes.Internal, "sometging went wrong, please check post info")
	}
	_, err = s.client.Review().DeletePostReview(ctx, &rs.GetReviewPostRequest{PostId: req.PostID})
	if err != nil {
		return &pb.Empty{}, err
	}
	return post, nil
}

// DeleteCustomerPost(*pb.CustomerId) (*pb.Empty,error)
func (s *PostService) DeleteCustomerPost(ctx context.Context, req *pb.CustomerId) (*pb.Empty, error) {
	post, err := s.storage.Post().DeleteCustomerPost(req)
	if err != nil {
		s.logger.Error("error delete post", l.Any("error delete post customer", err))
		return &pb.Empty{}, status.Error(codes.Internal, "sometging went wrong, please check post info")
	}
	return post, nil
}

func (s *PostService) ListPost(ctx context.Context, req *pb.ListPostRequest) (*pb.ListPostResponse, error) {
	post, err := s.storage.Post().ListPost(req.Value, req.Limit, req.Page)
	if err != nil {
		s.logger.Error("error get  post list", l.Any("error get post list", err))
		return &pb.ListPostResponse{}, status.Error(codes.Internal, "sometging went wrong, please check post info")
	}
	return post, nil
}
